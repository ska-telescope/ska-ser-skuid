====================================
SKA Unique Identifiers Documentation
====================================

Description
-----------

This package provides the ability to generate SKA Unique Identifiers (IDs) for differentiating entities that may be 
created and archived during telescope operation. Such entities include Schedule Bock Definitions, Data Products, etc.

Scan IDs
========

String of format ``localSeq`` where:

- **localSeq**: locally generated sequence: 15 digit string representation of a natural number (48 bits), with leading zeros.

An example of a scan ID: ``000000000001234``

IDs of other entity types
=========================

String of format ``type-generatorID-datetime-localSeq`` where:

- **type**: type of entity (object) the ID is for (SBD, SBI, etc.). Refer to class *EntityTypes*.
  
- **generatorID**: uniquely identifies the particular SKA ID generator service instance that issued the SKA UID.
  This can also be used to separate IDs that are generated for different purposes such as normal operations, testing, engineering, AIV.

- **datetime**: **ISO8601** format date (in UT time zone), in the standard, basic, format plus some precision of time - this can help with localising
  a debugging window if required. Note we are assuming a 1 day precision for the moment.

- **localSeq**: locally generated sequence: 5 digit string representation of a natural number, with leading zeros.

A possible example SKA UID for a Scheduling Block Instance generated at the Mid telescope could be: ``sbi-M001-20191031-01234``.
This example assumes a one day date precision and five digits for the sequence.


.. toctree::
  :maxdepth: 1

   Developer Guide<package/guide>
   API<api/index>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
