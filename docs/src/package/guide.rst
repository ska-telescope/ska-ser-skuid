========
Features
========

Scan IDs
^^^^^^^^
- Generates one scan ID per request.

  - Scan ID is unique to a single SKA UID service instance, beyond execution of the service instance, for a maximum of 2^48 scans.

  .. code-block:: gherkin

      Scenario: OET requests a scan ID
        Given skuid service is running
        When I request a scan ID
        Then The scan ID returned as an integer
        And The numerical value of the scan ID is greater than 0 and less or equal to 2^48

  .. code-block:: gherkin

      Scenario: OET requests a second scan ID
        Given I have already requested a scan ID
        When I request a scan ID
        Then the numerical value of the latest scan ID is one more than the previous scan ID

Other supported entity IDs
^^^^^^^^^^^^^^^^^^^^^^^^^^
- Generates one entity ID on request, with entity type specified in the request.
- Supports global uniqueness of entity IDs by encoding in the ID the identity of the unique SKA UID service instance as well as
  the type of entity, the date and time and a sequence number.
- Allows addition of an entity type to be supported.
- Persists supported entity types beyond execution of the SKA UID service instance.

.. code-block:: gherkin

    Scenario: OET requests an ID for a Scheduling Block Definition
      Given entity type Scheduling Block Definition is supported by the SKA UID service instance
      When I request an ID for a Scheduling Block Definition entity
      Then the ID returned is a string of the format type-generatorID-datetime-localSeq

Transaction IDs
^^^^^^^^^^^^^^^
- Transaction ID from the service

  - Generates one transaction ID per request.
  - The ID is retrieved from the Skuid service

- Local Transaction ID

  - Generates one transaction ID per request.
  - The ID is not retrieved from the skuid service and may have duplicates

.. code-block:: python

    import os

    from ska_ser_skuid.client import SkuidClient

    def get_transaction_id():
        if "SKUID_URL" in os.environ and os.environ["SKUID_URL"]:
            client = SkuidClient(os.environ["SKUID_URL"])
            return client.fetch_transaction_id()
        return SkuidClient.get_local_transaction_id()

    transaction_id = get_transaction_id()

.. code-block:: python

    # should the skuid service not respond for any reason
    # a locally generated transaction ID will be returned
    client = SkuidClient("non_existing_url")
    transaction_id = client.fetch_transaction_id()
    print(transaction_id)
    # txn-local-20200921-516590971

.. note:: When ``skuid`` is deployed to a cluster, the ``SKUID_URL`` variable should be defined and accessible from the client environment.
   For example, a common cluster with namespace separation should have ``SKUID_URL`` of the format:
   ``SKUID_URL: ska-ser-skuid-<helm release name>-svc.<k8s namespace>.svc.<cluster-domain>:9870``
