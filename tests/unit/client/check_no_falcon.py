import pytest


def test_no_falcon():
    """Make sure there's no falcon when testing client"""
    with pytest.raises(ModuleNotFoundError):
        import falcon  # noqa: F401


def test_no_fasteners():
    """Make sure there's no fasteneres when testing client"""
    with pytest.raises(ModuleNotFoundError):
        import fasteners  # noqa: F401
