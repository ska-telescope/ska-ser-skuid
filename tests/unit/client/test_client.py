from unittest import mock

from ska_ser_skuid.client import SkuidClient


def test_client_url():
    client = SkuidClient("non_existent_url")
    assert client.service_url == "http://non_existent_url"

    client = SkuidClient("http://non_existent_url")
    assert client.service_url == "http://non_existent_url"


def test_failed_remote_txn():

    client = SkuidClient("non_existent_url")
    transaction_id = client.fetch_transaction_id()
    assert transaction_id.startswith("txn-local-")


def test_remote_txn_id():

    with mock.patch("ska_ser_skuid.client.requests.get") as mocked_request:
        mocked_response = mock.MagicMock()
        mocked_response.raise_for_status.return_value = None
        mocked_response.json.return_value = (
            '{"transaction_id":"transaction_id_1"}'
        )
        mocked_request.return_value = mocked_response

        client = SkuidClient("non_existent_url")
        transaction_id = client.fetch_transaction_id()
        mocked_request.assert_called_with(
            "http://non_existent_url/skuid/ska_transaction_id"
        )
        assert transaction_id == "transaction_id_1"


def test_remote_skuid():

    with mock.patch("ska_ser_skuid.client.requests.get") as mocked_request:
        skuid = (
            '{"ska_uid": "sdp-t0001-20210122-00002", "generator_id": "t0001"}'
        )
        mocked_response = mock.MagicMock()
        mocked_response.raise_for_status.return_value = None
        mocked_response.json.return_value = skuid
        mocked_request.return_value = mocked_response

        client = SkuidClient("non_existent_url")
        skuid = client.fetch_skuid("sdp")
        mocked_request.assert_called_with(
            "http://non_existent_url/skuid/ska_id/sdp"
        )
        assert skuid == "sdp-t0001-20210122-00002"


def test_remote_scan_id():

    with mock.patch("ska_ser_skuid.client.requests.get") as mocked_request:
        scan_id = '{"scan_id": 2, "generator_id": "t0001"}'
        mocked_response = mock.MagicMock()
        mocked_response.raise_for_status.return_value = None
        mocked_response.json.return_value = scan_id
        mocked_request.return_value = mocked_response

        client = SkuidClient("non_existent_url")
        scan_id = client.fetch_scan_id()
        mocked_request.assert_called_with(
            "http://non_existent_url/skuid/ska_scan_id"
        )
        assert scan_id == 2
