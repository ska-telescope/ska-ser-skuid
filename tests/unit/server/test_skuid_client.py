import multiprocessing
import os
from datetime import date
from unittest import mock
from wsgiref import simple_server

import pytest
import requests
from requests.exceptions import ConnectionError, HTTPError, Timeout

from ska_ser_skuid import SkuidConfig
from ska_ser_skuid.client import SkuidClient
from ska_ser_skuid.server.http import app as SkuidHttpService
from ska_ser_skuid.services import SKAEntityTypeService

TEST_SERVER_PORT = 9837


@pytest.fixture(scope="module")
def skuid_httpd():
    httpd = simple_server.make_server("", TEST_SERVER_PORT, SkuidHttpService)
    child_proc = multiprocessing.Process(target=httpd.serve_forever)
    child_proc.start()
    yield
    child_proc.terminate()
    child_proc.join()


@pytest.fixture(scope="module")
def cursor_file():
    cursor_file = SkuidConfig().cursor_file
    with open(cursor_file, "w") as f:
        f.write(date.today().strftime("%Y%m%d") + "|0")
    yield
    os.remove(cursor_file)


@pytest.mark.usefixtures("skuid_httpd", "cursor_file")
@pytest.mark.parametrize(
    "entity_type", [t.value for t in SKAEntityTypeService.DefaultEntityTypes]
)
@mock.patch("requests.get", wraps=requests.get)
def test_skuid_client_able_to_get_new_skuid_for_each_entity_from_http_service(
    mock_get, entity_type
):
    service_url = f"localhost:{TEST_SERVER_PORT}"
    client = SkuidClient(service_url)

    my_skuid = client.fetch_skuid(entity_type)

    expected_request_path = f"http://{service_url}/skuid/ska_id/{entity_type}"
    mock_get.assert_called_with(expected_request_path)
    assert my_skuid.split("-")[0] == entity_type


@pytest.fixture(scope="module")
def scan_id_cursor_file():
    cursor_file = SkuidConfig().scan_id_cursor
    with open(cursor_file, "w") as f:
        f.write("0")
    yield
    os.remove(cursor_file)


@pytest.mark.usefixtures("skuid_httpd", "scan_id_cursor_file")
@mock.patch("requests.get", wraps=requests.get)
def test_skuid_client_able_to_get_new_scan_id_from_http_service(mock_get):
    service_url = f"localhost:{TEST_SERVER_PORT}"
    client = SkuidClient(service_url)

    my_scan_id = client.fetch_scan_id()

    expected_request_path = f"http://{service_url}/skuid/ska_scan_id"
    mock_get.assert_called_with(expected_request_path)
    assert my_scan_id == 1


@pytest.mark.usefixtures("skuid_httpd", "cursor_file")
@mock.patch("requests.get", wraps=requests.get)
def test_skuid_client_able_to_get_new_transaction_id_from_http_service(
    mock_get,
):
    service_url = f"localhost:{TEST_SERVER_PORT}"
    client = SkuidClient(service_url)

    transaction_id = client.fetch_transaction_id()

    expected_request_path = f"http://{service_url}/skuid/ska_transaction_id"
    mock_get.assert_called_with(expected_request_path)
    assert transaction_id.startswith("txn")


def test_skuid_client_able_to_get_new_transaction_id_when_http_service_fails(
    caplog,
):
    service_url = f"localhost:{TEST_SERVER_PORT}"
    client = SkuidClient(service_url)

    for error_response in [
        HTTPError("An HTTPError"),
        ConnectionError("An HTTPError"),
        Timeout("An HTTPError"),
    ]:
        with mock.patch("ska_ser_skuid.client.requests.get") as mock_get:
            test_side_effect = mock.MagicMock()
            test_side_effect.raise_for_status.side_effect = error_response
            mock_get.return_value = test_side_effect
            transaction_id = client.fetch_transaction_id()
            assert transaction_id.startswith("txn")
            assert "local" in transaction_id
            assert (
                "Problem retrieving transaction ID from the skuid service"
                in caplog.messages[-1]
            )


def test_invalid_skuid_url():
    client = SkuidClient("non_exiting_url")
    transaction_id = client.fetch_transaction_id()
    assert "local" in transaction_id
    assert transaction_id.startswith("txn")


def test_local_transaction_id():
    txn_id = SkuidClient.get_local_transaction_id()
    entity_type, service_id, the_date, seq_num = txn_id.split("-")
    assert entity_type == "txn"
    assert service_id == "local"
    assert the_date == date.today().strftime("%Y%m%d")
    assert len(seq_num) == 9
    assert (
        seq_num.isdigit()
    ), f"Sequence number is not a valid number, {seq_num}"
