import json
import os
import random
import string
from datetime import date

import falcon
import pytest
from falcon import testing

from ska_ser_skuid import CONFIG
from ska_ser_skuid.server import http
from ska_ser_skuid.services import (
    SKAEntityTypeService,
    SKALocalSeqService,
    SKAScanIDService,
)


@pytest.fixture()
def client():
    return testing.TestClient(http.create())


def teardown_module():
    cache_file_paths = [
        CONFIG.cursor_file,
        CONFIG.scan_id_cursor,
        CONFIG.types_file,
    ]
    cache_file_paths = [str(p) for p in cache_file_paths]
    lock_file_paths = [p + ".lock" for p in cache_file_paths]

    for p in cache_file_paths + lock_file_paths:
        os.remove(p) if os.path.exists(p) else None


def test_root_response(client):
    result = client.simulate_get("/")
    assert result.status == falcon.HTTP_200
    assert result.text == CONFIG.WELCOME_MSG


def test_on_get_new_skuid(client):
    local_seq_service = SKALocalSeqService(CONFIG.cursor_file)

    cursor = local_seq_service._read_cursor()
    previous_ordinal = cursor.split("|")[1]
    next_ordinal = str(int(previous_ordinal) + 1).zfill(5)
    datestamp = date.today().strftime("%Y%m%d")
    generator_id = CONFIG.generator_id

    want = f"sbi-{generator_id}-{datestamp}-{next_ordinal}"
    doc = {
        "ska_uid": want,
        "generator_id": CONFIG.generator_id,
    }
    result = client.simulate_get("/skuid/ska_id/sbi")
    assert result.json == json.dumps(doc)


def test_on_get_new_scan_id(client):
    local_seq_service = SKAScanIDService(CONFIG.scan_id_cursor)
    previous_ordinal = local_seq_service._read_cursor()
    next_ordinal = int(previous_ordinal) + 1

    want = f"{next_ordinal}"
    doc = {
        "scan_id": int(want),
        "generator_id": CONFIG.generator_id,
    }
    result = client.simulate_get("/skuid/ska_scan_id")
    assert result.json == json.dumps(doc)


def test_on_get_new_transaction_id(client):
    local_seq_service = SKALocalSeqService(CONFIG.cursor_file)

    cursor = local_seq_service._read_cursor()
    previous_ordinal = cursor.split("|")[1]
    next_ordinal = str(int(previous_ordinal) + 1).zfill(9)
    datestamp = date.today().strftime("%Y%m%d")
    generator_id = CONFIG.generator_id

    want = f"txn-{generator_id}-{datestamp}-{next_ordinal}"
    doc = {
        "transaction_id": want,
        "generator_id": CONFIG.generator_id,
    }
    result = client.simulate_get("/skuid/ska_transaction_id")
    assert result.json == json.dumps(doc)


def test_on_get_new_execution_block_id(client):
    local_seq_service = SKALocalSeqService(CONFIG.cursor_file)

    cursor = local_seq_service._read_cursor()
    previous_ordinal = cursor.split("|")[1]
    next_ordinal = str(int(previous_ordinal) + 1).zfill(5)
    datestamp = date.today().strftime("%Y%m%d")
    generator_id = CONFIG.generator_id

    want = f"eb-{generator_id}-{datestamp}-{next_ordinal}"
    doc = {
        "ska_uid": want,
        "generator_id": CONFIG.generator_id,
    }
    result = client.simulate_get("/skuid/ska_id/eb")
    assert result.json == json.dumps(doc)


def test_default_entity_types_are_preloaded(client):
    want = {t.value: t.name for t in SKAEntityTypeService.DefaultEntityTypes}
    result = client.simulate_get("/skuid/entity_types/get")

    assert json.loads(result.json) == want


def test_additional_entity_types_can_be_added_idempotently(client):
    new_type = ("nwt", "NewType")

    result = client.simulate_post(
        f"/skuid/entity_types/add/{new_type[0]}/{new_type[1]}",
        body=new_type[1],
    )
    result2 = client.simulate_post(
        f"/skuid/entity_types/add/{new_type[0]}/{new_type[1]}",
        body=new_type[1],
    )

    types_list = json.loads(result.json)

    assert result.status_code == 201  # new entity created
    assert result2.status_code == 200  # new entity not re-created but okay
    assert "nwt" in types_list


@pytest.mark.parametrize(
    "short_form,long_form",
    [
        (
            "".join(random.choice(string.ascii_lowercase) for _ in range(6)),
            "TooLongType",
        ),
        ("", "TooShortType"),
    ],
)
def test_invalid_types_cannot_be_added(client, short_form, long_form):
    result = client.simulate_post(
        f"/skuid/entity_types/add/{short_form}/{long_form}"
    )
    assert result.status_code == 400
