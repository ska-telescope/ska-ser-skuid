import json
import pathlib
import random
import string
import tempfile
from datetime import date, timedelta

import pytest

from ska_ser_skuid.services import (
    SKAEntityTypeService,
    SKALocalSeqService,
    SKAScanIDService,
)

MAX_SEQ_ORD_VALUE = 99999


def test_ensure_modules_available():
    import falcon  # noqa: F401
    import fasteners  # noqa: F401


class TestLocalSkaIDSeqService:
    def test_localseq_service_should_increment_last_generated_ordinal_when_called_on_same_date(
        self,
    ):
        fd, path = tempfile.mkstemp()

        with open(path, "w") as fd:
            datestamp = date.today().strftime("%Y%m%d")
            previous_ordinal = random.choice(
                [x for x in range(MAX_SEQ_ORD_VALUE)]
            )

            fd.write(f"{datestamp}|{previous_ordinal}")

        localseq_service = SKALocalSeqService(path)
        sequence = localseq_service.next_sequence()

        assert sequence == str(previous_ordinal + 1).zfill(5)

    def test_localseq_service_should_return_ordinal_value_of_one_when_date_is_different(
        self,
    ):
        fd, path = tempfile.mkstemp()

        with open(path, "w") as fd:
            yesterday_datestamp = (date.today() - timedelta(days=1)).strftime(
                "%Y%m%d"
            )
            previous_ordinal = random.choice(
                [x for x in range(MAX_SEQ_ORD_VALUE)]
            )

            fd.write(f"{yesterday_datestamp}|{previous_ordinal}")

        localseq_service = SKALocalSeqService(path)
        sequence = localseq_service.next_sequence()

        assert sequence == str(1).zfill(5)

    @pytest.mark.parametrize("invalid_cursor", ["invalid", "3jh4g23j4"])
    def test_localseq_service_should_throw_exception_when_cursor_file_is_invalid(
        self, invalid_cursor
    ):
        _, cursor_file = tempfile.mkstemp()

        with open(cursor_file, "w") as f:
            f.write(invalid_cursor)

        localseq_service = SKALocalSeqService(cursor_file)
        with pytest.raises(SKALocalSeqService.InvalidCursorFileError) as ex:
            localseq_service.next_sequence()

        assert ex.match(
            f"Cursor file contents are invalid. Should be: '<datestamp>|<ord>' but got '{invalid_cursor}'"
        )


class TestLocalSKAScanIDSeqService:
    def test_local_scanid_seq_should_increment_last_generated_ordinal(self):
        fd, path = tempfile.mkstemp()

        with open(path, "w") as fd:
            previous_ordinal = random.choice(
                [x for x in range(MAX_SEQ_ORD_VALUE)]
            )

            fd.write(f"{previous_ordinal}")

        local_scanid_seq_service = SKAScanIDService(path)
        sequence = local_scanid_seq_service.next_sequence()

        assert sequence == previous_ordinal + 1

    def test_local_scanid_seq_service_throws_exception_when_ordinal_exceeds_max_seq_number(
        self,
    ):
        max_seq_num = pow(2, 48) - 1

        fd, path = tempfile.mkstemp()

        with open(path, "w") as fd:
            fd.write(f"{max_seq_num}")

        local_scanid_seq_service = SKAScanIDService(path)

        with pytest.raises(AssertionError) as ex:
            local_scanid_seq_service.next_sequence()

        assert ex.match("Maximum INT for 48 bit number exceeded")


class TestSkaEntityTypeService:
    def test_entity_type_service_persists_entity_types_into_cache_file_when_file_is_empty(
        self,
    ):
        want = {
            t.value: t.name for t in SKAEntityTypeService.DefaultEntityTypes
        }
        _, path = tempfile.mkstemp()

        entitytype_service = SKAEntityTypeService(path)

        with open(path, "r") as f:
            file_contents = f.read()

        assert file_contents == json.dumps(want)
        assert entitytype_service.EntityTypes == want

    def test_entity_type_service_loads_entities_from_cache_file_when_file_is_not_empty(
        self,
    ):
        want = {
            "mut": "MadeUpType",
            "ima": "ImaginaryType",
            "tot": "TestOnlyType",
        }
        _, path = tempfile.mkstemp()

        with open(path, "w") as f:
            f.write(json.dumps(want))

        entitytype_service = SKAEntityTypeService(path)

        assert entitytype_service.EntityTypes == want

    def test_EntityTypes_should_always_be_synchronised_with_values_from_cache_file(
        self,
    ):
        _, path = tempfile.mkstemp()

        with open(path, "w") as f:
            want = {
                "tnt": "TestTypeA",
                "test": "TestTypeB",
                "tst": "TestTypeC",
            }
            f.write(json.dumps(want))

        entitytype_service = SKAEntityTypeService(path)

        assert entitytype_service.EntityTypes == want

    @pytest.mark.parametrize(
        "short_name,full_name",
        [
            (
                "".join(random.choice(string.ascii_letters) for _ in range(6)),
                "TooLongType",
            ),
            ("", "TooShortType"),
        ],
    )
    def test_save_type_should_throw_exception_when_short_form_character_length_exceeds_six(
        self, short_name, full_name
    ):
        entitytype_service = SKAEntityTypeService(tempfile.mkstemp()[1])

        with pytest.raises(SKAEntityTypeService.InvalidEntityTypeError) as ex:
            entitytype_service.save_type(short_name, full_name)

        ex.match(
            "Entity type needs to be between 1 and 6 characters in length."
        )

    def test_save_type_should_be_idempotent(self):
        types_file_path = pathlib.Path(tempfile.mkstemp()[1])
        new_type = ("new", "NewType")
        entitytype_service = SKAEntityTypeService(types_file_path)

        saved = entitytype_service.save_type(new_type[0], new_type[1])
        assert saved is True
        modified_at = types_file_path.stat().st_mtime

        for _ in range(3):
            saved = entitytype_service.save_type(new_type[0], new_type[1])
            assert not saved

        new_type_collection = {
            t.value: t.name for t in SKAEntityTypeService.DefaultEntityTypes
        }
        new_type_collection[new_type[0]] = new_type[1]

        assert types_file_path.stat().st_mtime == modified_at
        assert entitytype_service.EntityTypes == new_type_collection
