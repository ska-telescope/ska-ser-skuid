import pathlib
import random
import string
import tempfile

import pytest

from ska_ser_skuid.storage import CacheFileStore


@pytest.fixture(scope="function")
def nonexistent_file():
    return pathlib.Path(tempfile.mkdtemp()).joinpath(random_string())


def random_string(length=12):
    return "".join(random.choice(string.ascii_letters) for _ in range(length))


def test_cache_file_store_should_throw_expection_when_file_does_not_exist_and_create_is_disabled(
    nonexistent_file,
):
    with pytest.raises(FileNotFoundError) as ex:
        CacheFileStore(nonexistent_file, create=False)

    assert ex.match(f"Cache file not found at {nonexistent_file}")


def test_cache_file_store_should_create_empty_cache_file_by_default(
    nonexistent_file,
):
    CacheFileStore(nonexistent_file)

    assert nonexistent_file.exists()
