import os
import pathlib
import random
import string
import tempfile

import pytest

from ska_ser_skuid.config import (
    CURSOR_FILE_ENV_VAR,
    SCAN_ID_CURSOR_ENV_VAR,
    TYPES_FILE_ENV_VAR,
    SkuidConfig,
)


def test_skuid_config_should_be_a_singleton():
    skuid_config_a = SkuidConfig()
    skuid_config_b = SkuidConfig()

    assert skuid_config_a is skuid_config_b


def test_skuid_config_extracts_generator_id_from_environment_variable():
    generator_id = "".join(
        random.choice(string.ascii_letters) for _ in range(5)
    )
    os.environ["SKUID_GENERATOR_ID"] = generator_id

    skuid_config = SkuidConfig()
    assert skuid_config.generator_id == generator_id


def test_skuid_config_extracts_cursor_file_path_from_environment_variable():
    _, path = tempfile.mkstemp()
    os.environ["SKUID_CURSOR_FILE"] = path

    skuid_config = SkuidConfig()
    assert skuid_config.cursor_file == pathlib.Path(path)


def test_skuid_config_extracts_scan_id_cursor_file_path_from_environment_variable():
    _, path = tempfile.mkstemp()
    os.environ["SKUID_SCANID_CURSOR_FILE"] = path

    skuid_config = SkuidConfig()
    assert skuid_config.scan_id_cursor == pathlib.Path(path)


def test_skuid_config_extracts_types_file_path_from_environment_variable():
    _, path = tempfile.mkstemp()
    os.environ["SKUID_TYPES_FILE"] = path

    skuid_config = SkuidConfig()
    assert skuid_config.types_file == pathlib.Path(path)


@pytest.fixture(scope="function")
def new_file():
    _, path = tempfile.mkstemp()
    return pathlib.Path(path)


def test_cursor_lock_file_is_sibling_file_with_lock_extension(new_file):
    os.environ[CURSOR_FILE_ENV_VAR] = str(new_file)

    cursor_lock_file_path = SkuidConfig().cursor_file_lock
    assert cursor_lock_file_path.parent == SkuidConfig().cursor_file.parent
    assert cursor_lock_file_path.suffix == ".lock"


def test_types_file_is_sibling_of_cursor_file_in_dir_tree_when_unspecified(
    new_file,
):
    os.environ[CURSOR_FILE_ENV_VAR] = str(new_file)
    del os.environ[TYPES_FILE_ENV_VAR]

    types_file_path = SkuidConfig().types_file
    assert pathlib.Path(types_file_path).parent == new_file.parent


def test_types_lock_file_is_sibling_file_to_cursor_lock_with_types_file_name_and_lock_extension(
    new_file,
):
    os.environ[CURSOR_FILE_ENV_VAR] = str(new_file)
    os.environ[TYPES_FILE_ENV_VAR] = "/another/path/types_file"

    types_lock_file_path = SkuidConfig().types_file_lock
    assert types_lock_file_path.parent == SkuidConfig().cursor_file_lock.parent
    assert types_lock_file_path.stem == SkuidConfig().types_file.stem
    assert types_lock_file_path.suffix == ".lock"


def test_scan_id_cursor_file_is_sibling_of_cursor_file_when_unspecified():
    del os.environ[SCAN_ID_CURSOR_ENV_VAR]
    assert (
        SkuidConfig().scan_id_cursor.parent == SkuidConfig().cursor_file.parent
    )


def test_scan_id_cursor_lock_file_is_sibling_file_with_lock_extension(
    new_file,
):
    os.environ[SCAN_ID_CURSOR_ENV_VAR] = str(new_file)

    scan_id_cursor_lock_file_path = SkuidConfig().scan_id_cursor_lock
    assert (
        scan_id_cursor_lock_file_path.parent
        == SkuidConfig().scan_id_cursor_lock.parent  # noqa: W503
    )
    assert scan_id_cursor_lock_file_path.suffix == ".lock"
