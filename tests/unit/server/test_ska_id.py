import os
import tempfile
from datetime import date
from unittest.mock import Mock

import pytest

from ska_ser_skuid import SkuidConfig
from ska_ser_skuid.models import SkaScanID, SkaUID
from ska_ser_skuid.services import SKAEntityTypeService, SKALocalSeqService


@pytest.mark.parametrize(
    "entity_type",
    [t.value for t in SKAEntityTypeService.DefaultEntityTypes],
)
def test_SkaUID_should_encode_entity_type_into_id_string(entity_type):
    skuid = SkaUID(entity_type, Mock(), Mock())
    want = entity_type
    got = parse_type_from(skuid)

    assert got == want


def test_SkaUID_should_encode_generatorId_into_id_string():
    entity_type = "sbd"
    generator_id = "M0005"

    skuid = SkaUID(entity_type, generator_id, Mock())
    got = parse_generatorId_from(skuid)

    assert got == generator_id


def test_SkaUID_should_encode_generated_localSeq_into_id_string():
    entity_type = "sbd"
    local_seq = "00001"

    localseq_service = Mock()
    localseq_service.next_sequence.return_value = local_seq

    skuid = SkaUID(entity_type, Mock(), localseq_service)
    got = parse_localSeq_from(skuid)

    localseq_service.next_sequence.assert_called()
    assert got == local_seq


def test_SkaUID_should_encode_todays_date_into_id_string():
    datestamp = date.today().strftime("%Y%m%d")
    skuid = SkaUID("sbd", Mock(), Mock())

    got = parse_date_from(skuid)

    assert got == datestamp


def test_SkaUID_should_use_services_to_resolve_generatorId_and_localSeq():
    prev_ord = 3
    datestamp = date.today().strftime("%Y%m%d")
    cursor_file_path = create_cursor_file_with_ordinal(datestamp, prev_ord)
    os.environ["SKUID_GENERATOR_ID"] = "AnyId"

    entity_type = "sbd"
    local_seq = prev_ord + 1
    local_seq = str(local_seq).zfill(5)

    localseq_service = SKALocalSeqService(cursor_file_path)

    skuid = SkaUID(entity_type, SkuidConfig().generator_id, localseq_service)

    assert str(skuid) == f"sbd-AnyId-{datestamp}-{local_seq}"


def test_SkaScanID_should_encode_generated_localSeq_into_id_string():
    local_seq = "1".zfill(15)

    localseq_service = Mock()
    localseq_service.next_sequence.return_value = local_seq

    scan_id = SkaScanID(Mock(), localseq_service)
    got = str(scan_id)

    localseq_service.next_sequence.assert_called()
    assert got == local_seq


def create_cursor_file_with_ordinal(datestamp, ordinal_value):
    _, path = tempfile.mkstemp()

    with open(path, "w") as fd:
        fd.write(f"{datestamp}|{ordinal_value}")

    return path


def parse_date_from(skuid):
    return str(skuid).split("-")[2]


def parse_type_from(skuid):
    return str(skuid).split("-")[0]


def parse_generatorId_from(skuid):
    return str(skuid).split("-")[1]


def parse_localSeq_from(skuid):
    return str(skuid).split("-")[-1]
