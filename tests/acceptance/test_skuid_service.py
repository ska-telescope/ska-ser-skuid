import json
import os
import re

import pytest
import requests


@pytest.fixture
def skuid_url():
    skuid_url = os.getenv("SKUID_URL")
    return skuid_url


@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@pytest.mark.acceptance
def test_the_skuid_url_env_variable_is_set(skuid_url):
    assert skuid_url, "The SKUID_URL environment variable is not set."


@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@pytest.mark.acceptance
def test_skuid_is_up_and_serving(skuid_url):
    """Check that skuid is up and base path is as expected"""

    skuid_url = os.getenv("SKUID_URL")
    resp = requests.get(f"http://{skuid_url}")
    assert "Welcome to skuid" in resp.text

    resp = requests.get(f"http://{skuid_url}/skuid")
    assert "Welcome to skuid" in resp.text


@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@pytest.mark.acceptance
def test_skuid_ska_id_endpoint(skuid_url):
    resp = requests.get(f"http://{skuid_url}/skuid/ska_id/test")
    resp_json = json.loads(resp.json())
    assert resp_json["ska_uid"], "ska_uid is empty"
    assert resp_json["generator_id"], "generator_id is empty"


@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@pytest.mark.acceptance
def test_ska_scan_id_endpoint(skuid_url):
    resp = requests.get(f"http://{skuid_url}/skuid/ska_scan_id")
    resp_json = json.loads(resp.json())
    assert resp_json["scan_id"] == 2


@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@pytest.mark.acceptance
def test_skuid_entity_types(skuid_url):
    resp = requests.get(f"http://{skuid_url}/skuid/entity_types/get")
    number_of_default_entity_types = 8
    resp_json = json.loads(resp.json())
    assert len(resp_json) == number_of_default_entity_types


@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@pytest.mark.acceptance
def test_skuid_ska_transaction_id(skuid_url):
    resp = requests.get(f"http://{skuid_url}/skuid/ska_transaction_id")
    resp_json = json.loads(resp.json())
    assert (
        "transaction_id" in resp_json
    ), f"Could not get transaction_id in {resp_json}"


@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@pytest.mark.acceptance
def test_skuid_format(skuid_url):
    resp = requests.get(f"http://{skuid_url}/skuid/ska_id/test")
    resp_json = json.loads(resp.json())
    ska_uid = resp_json["ska_uid"]
    # Example of the ska_uid: test-mvp01-20200120-00001
    pattern = r"test-mvp01-(\d{4})(\d{2})(\d{2})-\d{5}"
    assert re.match(pattern, ska_uid)
