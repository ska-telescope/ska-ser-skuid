###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

## unreleased
*************
* Used ska-ser-sphinx-theme for documentation
* Updated project dependencies
* Excluded selector when dynamically provisioning a volume for a PVC

Version 3.3.3
*************

* No functionality changes, minor lint and Dockerfile updates.

Version 3.3.2
*************

* No functionality changes, updated the chart version to match with the release version.

Version 3.3.1
*************

* No functionality changes, added acceptance tests.

Version 3.3.0
*************

* No functionality changes.
* Upated repo structure and CI to be in line with SKA standards.

Version 3.2.1
*************

* No change, moving artefacts to a new repository https://artefact.skao.int/.

Version 3.2.0
*************

* Resolved sphinx build issue
* Added `eb` endpoint to API for execution block

Version 3.1.0
*************

* Changed dependency from `ska_logging` to `ska_ser_logging`.

Version 3.0.0
*************

Change packaging to use single namespace.
Usage like `from from ska.skuid.client import SkuidClient` changes
to `from ska_ser_skuid.client import SkuidClient`.

Version 2.0.0
*************

* Updated dependencies for client installs (default)

  * Remove `falcon` dependency

  * Remove `fasteners` dependency

* Fixed SyntaxError: JSON.parse: unexpected character at line 2 column 1 of the JSON data
* Update docs to refer to scan ID as int

Version 1.2.0
*************

Added support for transaction IDs.

- SkuidClient now includes
  - `fetch_transaction_id`, fetch a transaction ID from the SKUID service
    - Should the request fail, a locally generated ID will be returned
  - `get_local_transaction_id`, generate a transaction ID locally
- Added http://<server>/skuid/ska_transaction_id endpoint

Version 1.1.1
*************

Remove ``src/ska/__init__.py`` file to comply with PEP 420 packaging.

Version 1.1.0
*************

Modifying the SKUID format based on feedback from PI6 Demo 1.
The format is now ``{entity_type}-{datestamp}-{generator_id}-{local_seq}`` (all fields delimited by ``-``).

Additionally:

- Standardise environment vars with ``SKUID_`` prefix.
- Expose the path of the backing types file as environment variable ``SKUID_TYPES_FILE``.
- Default its associated lockfile to be a sibling of the ``SKUID_CURSOR_FILE``.

Version 1.0.0
*************

The first release of the SKA Unique Identifier Service supports the
following:

- Provisioning of an SKA Unique Identifier via HTTP API:

```
GET /skuid/ska_id/{entity_type}
```

- provisioning of a SKA Scan ID via HTTP API:

```
GET /skuid/ska_scan_id
```

- a client object to allow interaction with the above APIs by using
SKUID package as a library:

```python
from ska.skuid.client import SkuidClient
client = SkuidClient("http://skuid-integration-test:9870")
new_skuid = client.fetch_skuid('sbd')
new_scan_id = client.fetch_scan_id()
```

- dynamic adding and updating of entity_types via HTTP API:

```
POST /skuid/entity_types/add/{short_name}/{full_name}
```

and

```
GET /skuid/entity_types/get
```

Added
-----

* Empty Python project directory structure
