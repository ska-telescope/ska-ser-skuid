SHELL=/bin/bash
.SHELLFLAGS=-o pipefail -c

NAME=ska-ser-skuid

VERSION=$(shell grep -e "^version = s*" pyproject.toml | cut -d = -f 2 | xargs)
IMAGE=$(CAR_OCI_REGISTRY_HOST)/$(NAME)
DOCKER_BUILD_CONTEXT=.
DOCKER_FILE_PATH=Dockerfile

MINIKUBE ?= true ## Minikube or not

# Set the specific environment variables required for pytest
PYTHON_VARS_BEFORE_PYTEST ?= PYTHONPATH=.:./src
PYTHON_VARS_AFTER_PYTEST = -m '$(MARK)' --json-report --json-report-file=build/report.json --junitxml=build/report.xml

python-test: MARK = not acceptance

OCI_TAG = $(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)

CI_REGISTRY ?= registry.gitlab.com

# Use the previously built image when running in the pipeline
ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set skuid.image.image=$(NAME) \
	--set skuid.image.registry=$(CI_REGISTRY)/ska-telescope/$(NAME) \
	--set skuid.image.tag=$(OCI_TAG)
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/$(NAME)/$(NAME):$(OCI_TAG)
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	$(CUSTOM_VALUES)

# include makefile targets from the submodule
-include .make/helm.mk
-include .make/oci.mk
-include .make/base.mk
-include .make/python.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak
