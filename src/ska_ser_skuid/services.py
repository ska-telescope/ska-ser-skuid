import json
import types
from datetime import date
from enum import Enum

from ska_ser_skuid.storage import CacheFileStore


class SKALocalSeqService:
    """
    The service object that determines the next generated localSeq, based on a stored cursor.
    """

    SEQ_START = 1

    class InvalidCursorFileError(Exception):
        """Invalid cursor file error exception"""

    def __init__(self, store_or_file_path):
        self.cursor_store = (
            store_or_file_path
            if isinstance(store_or_file_path, CacheFileStore)
            else CacheFileStore(store_or_file_path)
        )
        if not self._read_cursor():
            self._update_cursor(self.SEQ_START)

    def _read_cursor(self):
        return self.cursor_store.read()

    def _update_cursor(self, ordinal):
        current_datestamp = date.today().strftime("%Y%m%d")
        new_cursor = f"{current_datestamp}|{ordinal}"
        self.cursor_store.write(new_cursor)

    def next_sequence(self):
        """generate next id in the sequence"""
        cursor = self._read_cursor()
        cursor_data = cursor.split("|")

        if len(cursor_data) != 2:
            raise self.InvalidCursorFileError(
                f"Cursor file contents are invalid. Should be: "
                f"'<datestamp>|<ord>' but got '{cursor_data}'"
            )

        previous_datestamp, previous_ordinal = cursor_data
        current_datestamp = date.today().strftime("%Y%m%d")
        new_ordinal = (
            int(previous_ordinal) + 1
            if previous_datestamp == current_datestamp
            else self.SEQ_START
        )
        self._update_cursor(new_ordinal)

        return str(new_ordinal).zfill(5)


class SKAScanIDService:
    """
    The service responsible for calculating the next Scan ID and enforces
    constraints on the format and upper/lower bounds.
    """

    SEQ_START = 1
    MAX_BIT_INT = 48
    MAX_SEQ_NUM = pow(2, MAX_BIT_INT) - 1

    def __init__(self, store_or_file_path):
        self.cursor_store = (
            store_or_file_path
            if isinstance(store_or_file_path, CacheFileStore)
            else CacheFileStore(store_or_file_path)
        )

        if not self._read_cursor():
            self._update_cursor(self.SEQ_START)

    def _read_cursor(self):
        return self.cursor_store.read()

    def _update_cursor(self, ordinal):
        new_cursor = f"{ordinal}"
        self.cursor_store.write(new_cursor)

    def next_sequence(self):
        """generate next id in the sequence"""
        previous_ordinal = self._read_cursor()
        new_ordinal = int(previous_ordinal) + 1
        assert (
            new_ordinal < self.MAX_SEQ_NUM
        ), "Maximum INT for 48 bit number exceeded"
        self._update_cursor(new_ordinal)
        return new_ordinal


class SKAEntityTypeService:
    """
    Service that manages the valid SKA Entity Type representations.
    On instantiation, first loads DefaultEntityTypes into memory and
    synchronised to a backing cache/store, but allows additions and
    modifications through the `save_type` method.
    """

    class InvalidEntityTypeError(Exception):
        """Invalid entity type error exception"""

    class DefaultEntityTypes(Enum):
        """Default entity types"""

        SchedulingBlockDefinition = "sbd"
        SchedulingBlockInstance = "sbi"
        DataProduct = "dp"
        ObservingProject = "opj"
        ObservingProposal = "opp"
        ProcessingBlock = "pb"
        TransactionID = "txn"
        ExecutionBlock = "eb"

    def __init__(self, store_or_file_path):
        self.types_store = (
            store_or_file_path
            if isinstance(store_or_file_path, CacheFileStore)
            else CacheFileStore(store_or_file_path)
        )
        self._initialise_cache()
        self.EntityTypes = types.MappingProxyType(self._memoized_types)

    def _initialise_cache(self):
        if self.types_store.is_empty:
            self._memoized_types = {
                t.value: t.name for t in self.DefaultEntityTypes
            }
            self._store_types()
        else:
            self._memoized_types = self._fetch_types()

    def _store_types(self):
        self.types_store.write(json.dumps(self._memoized_types))

    def _fetch_types(self):
        return json.loads(self.types_store.read())

    def save_type(self, short_name, full_name):
        """Save the entity type"""
        if not 1 < len(short_name) < 6:
            raise self.InvalidEntityTypeError(
                "Entity type needs to be between 1 and 6 characters in length."
                f" {short_name} is {len(short_name)}"
            )

        # don't save again (idempotency)
        if self._memoized_types.get(short_name) == full_name:
            return False

        # save in memory (optimisation)
        self._memoized_types[short_name] = full_name

        # then save in file
        self._store_types()
        return True
