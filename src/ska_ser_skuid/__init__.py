# -*- coding: utf-8 -*-

"""Module init code."""


__version__ = "3.3.3"

__author__ = "Team Karoo"
__email__ = "cam+karoo@ska.ac.za"

from ska_ser_skuid.config import SkuidConfig

CONFIG = SkuidConfig()

__all__ = ["CONFIG"]
