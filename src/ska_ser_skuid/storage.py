import pathlib


class CacheFileStore:
    """
    An object representing a cache/store backed by a file on the filesystem.
    """

    def __init__(self, file_path, lock_file_path=None, create=True):
        self.cache_file = pathlib.Path(file_path)

        if not self.cache_file.exists():
            if create:
                pathlib.Path(file_path).touch()
            else:
                raise FileNotFoundError(f"Cache file not found at {file_path}")
        self.lock_file = (
            lock_file_path if lock_file_path else f"{file_path}.lock"
        )

    @property
    def is_empty(self):
        """check is cache file is empty"""
        return self.cache_file.stat().st_size == 0

    def read(self):
        """read cache file contents"""
        try:
            # fasteners are used in server deployments,
            # but not for client usage
            import fasteners  # pylint: disable=import-outside-toplevel

            with fasteners.InterProcessLock(self.lock_file), open(
                self.cache_file, "r", encoding="utf8"
            ) as f:
                data = f.read()
        except ModuleNotFoundError:
            with open(self.cache_file, "r", encoding="utf8") as f:
                data = f.read()
        return data

    def write(self, data):
        """write out the cache file"""
        try:
            # fasteners are used in server deployments,
            # but not for client usage
            import fasteners  # pylint: disable=import-outside-toplevel

            with fasteners.InterProcessLock(self.lock_file), open(
                self.cache_file, "w", encoding="utf8"
            ) as fd:
                fd.write(data)
        except ModuleNotFoundError:
            with open(self.cache_file, "w", encoding="utf8") as fd:
                fd.write(data)
