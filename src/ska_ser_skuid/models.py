from datetime import date


class SkaUID:
    """
    Object that represents a SKA Unique Identifier.
    """

    def __init__(self, entity_type, generator_id, localseq_service):
        """Constructs the SkaUID.

        :param entity_type: an SKA Entity such as `sbd` for ScheduleBlockDefinition.
        :param generator_id: a unique identifier for the service generating this SKUID.
        :param localseq_service: the provider responsible for calculating the next localseq.
        """
        self.entity_type = entity_type
        self.generator_id = generator_id
        self.local_seq = localseq_service.next_sequence()
        self.today = date.today().strftime("%Y%m%d")

    def __str__(self):
        str_template = f"{self.entity_type}-{self.generator_id}-{self.today}-{self.local_seq}"
        return str_template


class SkaScanID:
    """
    Object that represents a Scan ID.
    """

    def __init__(self, generator_id, scanid_service):
        """Constructs the ScanID.

        :param generator_id: a unique identifier for the service generating this Scan ID.
        :param scanid_service: the provider responsible for calculating the next Scan ID.
        """
        self.generator_id = generator_id
        self.local_seq = scanid_service.next_sequence()

    def __str__(self):
        str_template = f"{self.local_seq}"
        return str_template.zfill(15)

    def __int__(self):
        return self.local_seq


class SkaTransactionID:
    """
    Object that represents a SKA Transaction ID.
    """

    def __init__(self, generator_id, localseq_service):
        """Constructs the SkaTransactionID.

        :param generator_id: a unique identifier for the service generating this SKUID.
        :param localseq_service: the provider responsible for calculating the next localseq.
        """
        self.entity_type = "txn"
        self.generator_id = generator_id
        self.local_seq = str(localseq_service.next_sequence()).rjust(9, "0")
        self.today = date.today().strftime("%Y%m%d")

    def __str__(self):
        str_template = f"{self.entity_type}-{self.generator_id}-{self.today}-{self.local_seq}"
        return str_template
