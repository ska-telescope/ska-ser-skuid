import json

import falcon

from ska_ser_skuid import CONFIG
from ska_ser_skuid.models import SkaScanID, SkaTransactionID, SkaUID
from ska_ser_skuid.services import (
    SKAEntityTypeService,
    SKALocalSeqService,
    SKAScanIDService,
)
from ska_ser_skuid.storage import CacheFileStore


class RootResource:
    """http root resource"""

    @staticmethod
    def on_get(_request, response):
        """resource http get method"""
        response.status = falcon.HTTP_200  # This is the default status
        response.body = CONFIG.WELCOME_MSG
        response.content_type = falcon.MEDIA_TEXT


class LocalSKAIDSeqdResource:
    """http local sequence service resource"""

    def __init__(self, local_ska_id_service):
        self.local_seq_service = local_ska_id_service

    def on_get(self, _request, response, entity_type):
        """resource local sequence service http get method"""
        skuid = SkaUID(
            entity_type, CONFIG.generator_id, self.local_seq_service
        )
        resp = {
            "ska_uid": str(skuid),
            "generator_id": CONFIG.generator_id,
        }
        response.media = json.dumps(resp)


class LocalSKAScanIDSeqdResource:
    """http local scan sequence service resource"""

    def __init__(self, scanid_service):
        self.scanid_service = scanid_service

    def on_get(self, _request, response):
        """resource local scan sequence service http get method"""
        scan_id = SkaScanID(CONFIG.generator_id, self.scanid_service)
        resp = {
            "scan_id": int(scan_id),
            "generator_id": CONFIG.generator_id,
        }
        response.media = json.dumps(resp)


class LocalEntityTypeResource:
    """http local entity type resource"""

    def __init__(self, entitytype_service):
        self.entitytype_service = entitytype_service

    def on_get(self, _request, response):
        """resource local entity type service http get method"""
        response.media = json.dumps(dict(self.entitytype_service.EntityTypes))

    def on_post(self, _request, response, short_name, full_name):
        """resource local entity type service http post method"""
        try:
            saved = self.entitytype_service.save_type(short_name, full_name)
            response.status = falcon.HTTP_201 if saved else falcon.HTTP_200
            response.media = json.dumps(
                dict(self.entitytype_service.EntityTypes), sort_keys=True
            )
        except SKAEntityTypeService.InvalidEntityTypeError as e:
            raise falcon.HTTPBadRequest(
                title="Invalid SKA Entity Type", description=str(e)
            )
        except Exception as e:
            raise falcon.HTTPError(
                status="500 Server Error", description=str(e)
            )


class LocalSKATransactionIDSeqdResource:
    """http local transaction id sequence resource"""

    def __init__(self, local_ska_id_service):
        self.local_seq_service = local_ska_id_service

    def on_get(self, _request, response):
        """resource local transaction id sequence service http get method"""
        skuid = SkaTransactionID(CONFIG.generator_id, self.local_seq_service)
        resp = {
            "transaction_id": str(skuid),
            "generator_id": CONFIG.generator_id,
        }
        response.media = json.dumps(resp)


def create():  # pylint: disable=too-many-locals
    """create application"""
    # caches
    cursor_store = CacheFileStore(CONFIG.cursor_file, CONFIG.cursor_file_lock)
    scanid_cursor_store = CacheFileStore(
        CONFIG.scan_id_cursor, CONFIG.scan_id_cursor_lock
    )
    entitytypes_store = CacheFileStore(
        CONFIG.types_file, CONFIG.types_file_lock
    )

    # services
    localseq_service = SKALocalSeqService(cursor_store)
    ska_id_service = SKAScanIDService(scanid_cursor_store)
    entitytypes_service = SKAEntityTypeService(entitytypes_store)

    # resources
    root_resource = RootResource()
    ska_id_resource = LocalSKAIDSeqdResource(localseq_service)
    ska_scan_id_resource = LocalSKAScanIDSeqdResource(ska_id_service)
    entity_type_resource = LocalEntityTypeResource(entitytypes_service)
    ska_transaction_id_resource = LocalSKATransactionIDSeqdResource(
        localseq_service
    )

    # Serve off /skuid for ingress in k8s
    url_prefix = "/skuid"
    routes = [
        ("/", root_resource),
        (url_prefix, root_resource),
        (f"{url_prefix}/ska_id/{{entity_type}}", ska_id_resource),
        (f"{url_prefix}/ska_scan_id", ska_scan_id_resource),
        (f"{url_prefix}/ska_transaction_id", ska_transaction_id_resource),
        (
            f"{url_prefix}/entity_types/add/{{short_name}}/{{full_name}}",
            entity_type_resource,
        ),
        (f"{url_prefix}/entity_types/get", entity_type_resource),
    ]

    api = falcon.API()
    for url, resource in routes:
        api.add_route(url, resource)

    return api


app = create()
