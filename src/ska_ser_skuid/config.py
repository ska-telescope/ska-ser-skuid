import os
import pathlib

GENERATOR_ID_ENV_VAR = "SKUID_GENERATOR_ID"
CURSOR_FILE_ENV_VAR = "SKUID_CURSOR_FILE"
SCAN_ID_CURSOR_ENV_VAR = "SKUID_SCANID_CURSOR_FILE"
TYPES_FILE_ENV_VAR = "SKUID_TYPES_FILE"


class Singleton(type):
    """SkuidConfig singleton"""

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                *args, **kwargs
            )
        return cls._instances[cls]


class SkuidConfig(metaclass=Singleton):
    """
    A configuration object that holds global options.
    """

    WELCOME_MSG = "\nWelcome to skuid\n\nPlease use /skuid/ska_id/{entity_type} to retrieve a ID"

    @property
    def generator_id(self):
        """obtain the generator id from env var GENERATOR_ID_ENV_VAR"""
        return os.environ.get(GENERATOR_ID_ENV_VAR, "default_generator_id")

    # Cursor
    @property
    def cursor_file(self):
        """get the cursor file path"""
        if CURSOR_FILE_ENV_VAR in os.environ:
            cursor_file_path = pathlib.Path(os.environ[CURSOR_FILE_ENV_VAR])
        else:
            current_path = pathlib.Path(__file__).parent.absolute()
            cursor_file_path = current_path.parent.joinpath(
                "cursor_file"
            ).absolute()

        return cursor_file_path

    @property
    def cursor_file_lock(self):
        """get the cursor lock file"""
        return pathlib.Path(f"{self.cursor_file}.lock")

    # Types
    @property
    def types_file(self):
        """get the types file path"""
        if TYPES_FILE_ENV_VAR in os.environ:
            types_file_path = pathlib.Path(os.environ.get(TYPES_FILE_ENV_VAR))
        else:
            # Just put it in the same place as the cursor file
            cursor_dir = pathlib.Path(self.cursor_file).parent.absolute()
            types_file_path = cursor_dir.joinpath("types_file").absolute()

        return types_file_path

    @property
    def types_file_lock(self):
        """get the types file lock path"""
        return pathlib.Path(
            f"{self.cursor_file.parent}/{self.types_file.stem}.lock"
        )

    # Scan ID
    @property
    def scan_id_cursor(self):
        """get the scan id cursor file path"""
        if SCAN_ID_CURSOR_ENV_VAR in os.environ:
            scan_id_cursor_file_path = pathlib.Path(
                os.environ[SCAN_ID_CURSOR_ENV_VAR]
            )
        else:
            # Just put it in the same place as the cursor file
            cursor_dir = self.cursor_file.parent.absolute()
            scan_id_cursor_file_path = cursor_dir.joinpath(
                "scan_id_cursor_file"
            ).absolute()

        return scan_id_cursor_file_path

    @property
    def scan_id_cursor_lock(
        self,
    ):
        """get the scan id cursor lock path"""
        return pathlib.Path(f"{self.scan_id_cursor}.lock")
