import json
import logging
import random
from datetime import date

import requests
import requests.exceptions
from ska_ser_logging import configure_logging

from ska_ser_skuid.services import SKAEntityTypeService

configure_logging()
logger = logging.getLogger("ska_ser_skuid")


class SkuidClient:
    """
    A client for interacting with a remote SKUID service.
    """

    def __init__(self, skuid_url):
        """Instantiates a new SKUID client.

        :param skuid_url: The URL of the remote SKUID service to use.
        """
        if skuid_url.startswith("http"):
            self.service_url = skuid_url
        else:
            self.service_url = "http://" + skuid_url

    def fetch_skuid(self, entity_type):
        """Fetches a new SKA Unique ID from the SKUID service

        :param entity_type: the EntityType that the SKUID is for,
            e.g. `sbd` for ScheduleBlockDefinition
        :return: a new SKA Unique ID as a string
        """
        entity_type = (
            entity_type.value
            if isinstance(entity_type, SKAEntityTypeService.DefaultEntityTypes)
            else entity_type
        )
        resp = requests.get(f"{self.service_url}/skuid/ska_id/{entity_type}")
        return json.loads(resp.json())["ska_uid"]

    def fetch_scan_id(self):
        """Fetches a new Scan ID from the SKUID service.

        :return: a new Scan ID from the SKUID service as a string
        """
        resp = requests.get(f"{self.service_url}/skuid/ska_scan_id")
        return json.loads(resp.json())["scan_id"]

    def fetch_transaction_id(self):
        """Fetches a new Transaction ID from the SKUID service.

        Should the fetch from the service fail with a 4xx or 5xx
        then a locally generated ID will be returned.

        :return: a new Transaction ID from the SKUID service as a string
        """
        try:
            resp = requests.get(f"{self.service_url}/skuid/ska_transaction_id")
            resp.raise_for_status()
        except (
            requests.exceptions.ConnectionError,
            requests.exceptions.Timeout,
            requests.exceptions.HTTPError,
        ):
            logger.exception(
                "Problem retrieving transaction ID from the skuid service."
            )
            return self.get_local_transaction_id()
        else:
            return json.loads(resp.json())["transaction_id"]

    @classmethod
    def get_local_transaction_id(cls):
        """Return a transaction id that is not retrieved from the SKUID service.
        The possibility of duplicates exists.

        :return: generates a Transaction ID and returns as a string
        """
        random_seq = str(random.randint(1, 999999999)).rjust(9, "0")
        today = date.today().strftime("%Y%m%d")
        return f"txn-local-{today}-{random_seq}"
