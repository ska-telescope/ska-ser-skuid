ARG from_image=python:3.10

# Setup
FROM $from_image

WORKDIR /app

RUN pip install --upgrade pip
RUN pip install poetry==1.3.2

COPY . /app/

RUN poetry config virtualenvs.create false && poetry install

# Start service
EXPOSE 9870

RUN pip3 install gunicorn

ENV SKUID_GENERATOR_ID t0001
ENV CURSOR_FILE_PATH /app/cursor_file

CMD ["gunicorn", "--threads=1", "--workers=3", "-b", "0.0.0.0:9870", "ska_ser_skuid.server.http:app"]
